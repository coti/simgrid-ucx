
#include "ucp/api/ucp.h"
#include "ucp/api/ucp_compat.h"
#include "ucp/api/ucp_version.h"
#include "ucp/core/ucp_worker.h"
#include "ucs/async/async.h"



// #include "s4ucx/ucp_worker.hpp"
#ifndef S4U_ACTOR_H
#define S4U_ACTOR_H
#include <simgrid/actor.h>
#endif

// This declares a logging channel so that XBT_INFO can be used later
// XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_actor_create, "The logging channel used in this example");

unsigned ucp_worker_progress(ucp_worker_h worker) {
    unsigned count;

    // /* worker->inprogress is used only for assertion check.
    // * coverity[assert_side_effect]
    // */
    // UCP_WORKER_THREAD_CS_ENTER_CONDITIONAL(worker);

    // /* check that ucp_worker_progress is not called from within ucp_worker_progress */
    // ucs_assert(worker->inprogress++ == 0);
    // count = uct_worker_progress(worker->uct);
    // ucs_async_check_miss(&worker->async);

    // /* coverity[assert_side_effect] */
    // ucs_assert(--worker->inprogress == 0);

    // UCP_WORKER_THREAD_CS_EXIT_CONDITIONAL(worker);

    // return count;
    return 0;
}

void ucp_request_free(void *request) {
    
}

ucs_status_t ucp_worker_create(ucp_context_h context, 
                               const ucp_worker_params_t* params, 
                               ucp_worker_h* worker_p) {
    ucs_thread_mode_t thread_mode, uct_thread_mode;
    unsigned name_length;
    ucp_worker_h worker;
    ucs_status_t status;
    
    *worker_p = worker;
    return UCS_OK;
}

ucs_status_t ucp_worker_query(ucp_worker_h worker, ucp_worker_attr_t* attr) {
    ucs_status_t status = UCS_OK;
    uint32_t address_flags;

    if (attr->field_mask) {
        printf("ucp_worker_query?\n");
        // attr->thread_mode = ucp_worker_get_thread_mode(worker->flags);
        attr->thread_mode = UCS_THREAD_MODE_SINGLE;
    }

    return status;
}

void ucp_worker_destroy(ucp_worker_h worker) {
    // worker->actor->kill(); // TODO: how to know this is the one the worker owns
    // UCS_ASYNC_BLOCK(&worker->async);
    // ucs_free(worker);
}

ucs_status_t ucp_worker_get_address(ucp_worker_h worker, 
                                    ucp_address_t** address_p, 
                                    size_t* address_length_p) {
    ucs_status_t status;

    UCP_WORKER_THREAD_CS_ENTER_CONDITIONAL(worker); // TODO: diff with UCS_ASYNC_BLOCK ?

    /*
    // ucp_worker_address_pack is in ucp/core/ucp_worker.c
    status = ucp_worker_address_pack(worker, 0, address_length_p,
                                     (void**)address_p);
                                     */

    UCP_WORKER_THREAD_CS_EXIT_CONDITIONAL(worker);

    return status;
}

void ucp_worker_release_address(ucp_worker_h worker, ucp_address_t *address) {
    // ucs_free(address);
}


