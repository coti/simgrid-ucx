/**
 * Copyright (c) NVIDIA CORPORATION & AFFILIATES, 2001-2019. ALL RIGHTS RESERVED.
 * Copyright (C) ARM Ltd. 2016.  ALL RIGHTS RESERVED.
 *
 * See file LICENSE for terms.
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include "ucp/core/ucp_context.h"



void ucp_config_print(const ucp_config_t *config, FILE *stream,
                      const char *title, ucs_config_print_flags_t print_flags)
{
    printf("in ucp_config_print\n");
    // ucs_config_parser_print_opts(stream, title, config, ucp_config_table,
    //                              NULL, UCS_DEFAULT_ENV_PREFIX, print_flags);
    // ucp_config_print_cached_uct(config, stream, title, print_flags);
}

ucs_status_t ucp_config_read(const char *env_prefix, const char *filename,
                             ucp_config_t **config_p)
{
    return UCS_OK;
    // unsigned full_prefix_len = sizeof(UCS_DEFAULT_ENV_PREFIX);
    // unsigned env_prefix_len  = 0;
    // ucp_config_t *config;
    // ucs_status_t status;

    // config = ucs_malloc(sizeof(*config), "ucp config");
    // if (config == NULL) {
    //     status = UCS_ERR_NO_MEMORY;
    //     goto err;
    // }

    // if (env_prefix != NULL) {
    //     env_prefix_len   = strlen(env_prefix);
    //     /* Extra one byte for underscore _ character */
    //     full_prefix_len += env_prefix_len + 1;
    // }

    // config->env_prefix = ucs_malloc(full_prefix_len, "ucp config");
    // if (config->env_prefix == NULL) {
    //     status = UCS_ERR_NO_MEMORY;
    //     goto err_free_config;
    // }

    // if (env_prefix_len != 0) {
    //     ucs_snprintf_zero(config->env_prefix, full_prefix_len, "%s_%s",
    //                       env_prefix, UCS_DEFAULT_ENV_PREFIX);
    // } else {
    //     ucs_snprintf_zero(config->env_prefix, full_prefix_len, "%s",
    //                       UCS_DEFAULT_ENV_PREFIX);
    // }

    // TODO: fix
    // status = ucs_config_parser_fill_opts(config,
    //                                      UCS_CONFIG_GET_TABLE(ucp_config_table),
    //                                      config->env_prefix, 0);
    // if (status != UCS_OK) {
    //     goto err_free_prefix;
    // }

//     ucs_list_head_init(&config->cached_key_list);

//     *config_p = config;
//     return UCS_OK;

// err_free_prefix:
//     ucs_free(config->env_prefix);
// err_free_config:
//     ucs_free(config);
// err:
//     return status;
}