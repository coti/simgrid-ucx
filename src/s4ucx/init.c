
#include "ucp/api/ucp.h"
#include "ucp/core/ucp_context.h"

#ifndef S4U_ENGINE_H
#define S4U_ENGINE_H
#include <simgrid/engine.h>
#endif

// using namespace simgrid::s4u;


ucs_status_t ucp_init_version(unsigned api_major_version, unsigned api_minor_version,
                              const ucp_params_t *params, const ucp_config_t *config,
                              ucp_context_h *context_p) {
    printf("in ucp_init_version\n");

    // create simulation engine
    simgrid_init(params->argc, params->argv);
    // (*context_p)->engine = engine; // errors if using dummy argc argv

    // TODO: use getenv() to get platform file
    printf("after\n");  
    simgrid_load_platform("../small_platform.xml");

    // int* argc;
    // char** argv;
    // Engine engine(argc, argv);
    // std::cout << "after\n";
    // (*context_p)->engine = &engine;

    // std::cout << engine.is_initialized() << " " << engine.get_actor_count() << "\n";

    return UCS_OK;
}