
#include "ucp/api/ucp_def.h"
#include "ucp/api/ucp.h"
#include "ucp/core/ucp_ep.h"
#include "ucp/core/ucp_types.h"
#include "ucp/core/ucp_request.h"
#include "ucs/type/status.h"

// COPIED

ucs_status_t ucp_request_check_status(void *request)
{
    ucp_request_t *req = (ucp_request_t*)request - 1;

    if (req->flags & UCP_REQUEST_FLAG_COMPLETED) {
        ucs_assert(req->status != UCS_INPROGRESS);
        return req->status;
    }
    return UCS_INPROGRESS;
}

// ORIGINAL

ucs_status_ptr_t ucp_ep_close_nbx(ucp_ep_h ep, const ucp_request_param_t *param) {
    void* request = NULL;
    return request;
}

ucs_status_t ucp_ep_create(ucp_worker_h worker, const ucp_ep_params_t* params, 
                           ucp_ep_h* ep_p) {
    ucs_status_t test;
    return test;
}

void ucp_ep_print_info(ucp_ep_h ep, FILE* stream) {
    printf("ucp_ep_print_info\n");
}
